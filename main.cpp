#include <mbed.h>
#include "mslm/motor2.h"

AnalogOut RefOut(p18);
AnalogIn RefIn(p19);

StepMotor rm(p28,p29,p27,true,p30);
StepMotor lm(p23,p24,p25,true,p26);

BusOut ledbus(LED1,LED2,LED3,LED4);

DigitalOut led1(LED1);
DigitalOut led2(LED2);
DigitalOut led3(LED3);
DigitalOut led4(LED4);

DigitalIn sww(p8);
DigitalIn swr(p7);
DigitalIn swg(p6);
DigitalIn swb(p5);

int main(void){

  int sp=400;

  RefOut = 0.07/3.3;
  //RefOut = 0.001;


  //while(1){
   //if(!sww) led1 = 1;
    //else if(!swr) led2 = 1;
    //else if(!swg) led3 = 1;
    //else if(!swb) led4 = 1;
    //else ledbus = 0b1000;
  //

  led1 = 1;
  wait(0.5);
  led2 = 1;
  wait(0.5);
  led3 = 1;
  wait(0.5);
  led4 = 1;

  while(1){
   if(!sww) {
     rm.Start();
     lm.Start();

     wait(0.5);

     rm.update(sp);
     lm.update(sp);
   }
    else if(!swr) {
      sp+=100;
      rm.update(sp);
      lm.update(sp);
    }
    else if(!swg) {
      sp-=100;
      rm.update(sp);
      lm.update(sp);
    }
    else if(!swb) {
      rm.update(0);
      lm.update(0);
      rm.kill();
      lm.kill();
    }
    else ledbus = 0b1000;

   wait(0.1);


  }
}
